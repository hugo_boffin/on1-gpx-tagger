package de.sommercom.on1gpxtagger;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jenetics.jpx.GPX;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.measure.CoordinateFormat;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    private static Pattern dmsPattern = Pattern.compile("([0-9]+)°([0-9]+)'([0-9]+).([0-9]+)\" ([NSEW])");

    public static void main(String[] args) throws Exception {
        // TODO: Zeitoffset -

        // Find all *.on1 files and read them
        //      Compare timestamp to GPX positon. Do a lerp
        //      Do Backup of original image
        //      Interpolate Position -> convert to  long/lat degree(gt?jts?) -> Write To ON1

        // Copy some test images with and without gps and Developments
        // Test - Compare Images with existing GPS Tags
        // Test - Compare ON1 File Layout
        // Test - Reload Data from ON1 in ON1
        // Test - DO BACKUPS! Find files with developments

        List<GpsPoint> points = getPointsFromCSV();


//        Path path = Path.of("D:\\Fotos\\2014\\02_bis_03_NZ\\2014-02-27");
        Path path = Path.of("D:\\Fotos\\2014\\02_bis_03_NZ\\");
        List<Path> sideFiles = Files.walk(path)
                .filter(f -> f.toString().endsWith(".on1"))
                .collect(Collectors.toList());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd HH:mm:ss yyyy", Locale.US);
        final ObjectMapper mapper = new ObjectMapper();

        final CoordinateReferenceSystem wgs84 = CRS.decode("EPSG:4326");

        CoordinateFormat coordinateFormat = new CoordinateFormat(Locale.US, wgs84);
        coordinateFormat.setAnglePattern("D°M'S.ssssss\" ");

        AtomicInteger bilderOK = new AtomicInteger();
        AtomicInteger bilderUngenau = new AtomicInteger();
        AtomicInteger bilderUnbekannt = new AtomicInteger();
        sideFiles.stream().forEach(f -> {
            try {
                HashMap<String, HashMap> hashMap = mapper.readValue(f.toFile(), HashMap.class);
                HashMap<String, HashMap> photos = (HashMap<String, HashMap>) hashMap.get("photos");
                photos.entrySet().forEach(kv -> {
                    String imageFileName = (String) kv.getValue().get("name");
                    HashMap<String, Object> metadata = (HashMap<String, Object>) kv.getValue().computeIfAbsent("metadata", k -> new HashMap<String, Object>());
                    String rawGPS = (String) metadata.computeIfAbsent("GPS", k -> "");
                    String rawCaptureDate = (String) (metadata.computeIfAbsent("CaptureDate", k -> ""));
                    LocalDateTime captureDate = rawCaptureDate.isBlank() ? null : LocalDateTime.parse(rawCaptureDate.substring(rawCaptureDate.indexOf(" ") + 1), formatter);

                    // TODO: Bilder ohne metadata nicht vergessen - sollte passen

                    String calcGPS = "";
                    double distance = Double.NaN;
                    double[] decRawGPS = new double[]{Double.NaN, Double.NaN};
                    double[] lerpedCoordinateForDate = new double[]{Double.NaN, Double.NaN};
                    if(captureDate != null) {
                        // TODO: Hashmap direkt wieder verwenden und tags anpassen/hinzufügen

//                        lerpedCoordinateForDate = getLerpedCoordinateForDate(captureDate.minusHours(14), points);
                        lerpedCoordinateForDate = getLerpedCoordinateForDate(captureDate.minusHours(2), points);
                        if (lerpedCoordinateForDate != null) {
                            var dp = new DirectPosition2D(wgs84, lerpedCoordinateForDate[0], lerpedCoordinateForDate[1]);
                            calcGPS = coordinateFormat.format(dp);
//                        	calcGPS = lerpedCoordinateForDate[0] + ", "+lerpedCoordinateForDate[1];

                            if (rawGPS != null && !rawGPS.isBlank()) {
                                decRawGPS = dmsToDecimal(rawGPS);

                                double dx = decRawGPS[0] - lerpedCoordinateForDate[0];
                                double dy = decRawGPS[1] - lerpedCoordinateForDate[1];
                                distance = Math.sqrt(dx * dx + dy * dy);

                            }
                        } else {
                        	lerpedCoordinateForDate = new double[]{Double.NaN, Double.NaN};
                        }
                    }

                    System.out.println(path.relativize(f) + " -> " + imageFileName + " -> " + captureDate + " -> " + decRawGPS[0] + "," + decRawGPS[1] + " <-> " + lerpedCoordinateForDate[0] + "," + lerpedCoordinateForDate[1] + " Fehler:" + distance);

                    boolean ok = distance < 1E-8;

                    if (ok || !calcGPS.isBlank()) {
                        try {
//                            Files.copy(f, Path.of(f.toString()+".bak"), StandardCopyOption.REPLACE_EXISTING );
                            metadata.put("GPS", calcGPS);
                            mapper.writeValue(f.toFile(), hashMap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if (!calcGPS.isBlank() && distance != Double.NaN) {
                        if (ok) {
                            bilderOK.getAndIncrement();
                        } else {
                            bilderUngenau.getAndIncrement();
                        }
                    } else {
                        bilderUnbekannt.getAndIncrement();
                    }
//                    System.exit(0);

                });
            } catch (IOException e) {
                e.printStackTrace();
            }

//            String jsonResult = mapper.writerWithDefaultPrettyPrinter()
//                    .writeValueAsString(map);

        });

        System.out.println("Bilder OK: " + bilderOK);
        System.out.println("Bilder ungenau: " + bilderUngenau);
        System.out.println("Bilder unbekannt: " + bilderUnbekannt);


    }

    public static double[] dmsToDecimal(String rawGPS) {
        int nN = rawGPS.indexOf("N");
        int nS = rawGPS.indexOf("S");
        int splitter = nN == -1 ? nS : nN;

        String rawLat = rawGPS.substring(0, splitter + 1);
        String rawLong = rawGPS.substring(splitter + 1);

        return new double[]{dmsToDecimalVal(rawLat),
                dmsToDecimalVal(rawLong)
        };
    }

    private static double dmsToDecimalVal(String val) {
        Matcher matcher1 = dmsPattern.matcher(val.trim());
        
        if (!matcher1.matches() ) {
        	System.out.println("No match for: "+val.trim());
        	return Double.NaN;
        }

        double degrees = Double.parseDouble(matcher1.group(1));
        double minutes = Double.parseDouble(matcher1.group(2));
        double seconds = Double.parseDouble(matcher1.group(3) + "." + matcher1.group(4));
//        double secondsFrac = Double.parseDouble(matcher1.group(4));
        String rawSignum = matcher1.group(5);
        double signum = (rawSignum.equals("S") || rawSignum.equals("W")) ? -1 : +1;
        double decimal = degrees + minutes / 60.0 + (seconds) / 3600.0;// + secondsFrac / 3600.0;
        return signum * decimal;
    }

    private static String decimalToDegree(double decimalDegrees) {
        double degrees = Math.floor(decimalDegrees);
        double minutes = Math.floor(60.0 * (decimalDegrees - degrees));
        return (int) degrees + "°" + (int) minutes + "'";
        // 36°51'4.910889" S 174°46'39.426270" E
    }

    public static List<GpsPoint> getPointsFromCSV() throws IOException {
        return Files.lines(Path.of("src\\main\\resources\\gps_nz.csv"))
                .skip(1)
                .map(l -> {
                    String[] split = l.split(",");
                    return new GpsPoint(LocalDateTime.parse(split[0]),
                            Double.valueOf(split[1]),
                            Double.valueOf(split[2]),
                            Double.valueOf(split[3]),
                            split.length == 5 ? Double.valueOf(split[4]) : null
                    );

                }).collect(Collectors.toList());
    }

    public static double[] getLerpedCoordinateForDate(LocalDateTime imageTime, List<GpsPoint> points) {
        // TODO: Balancierter Baum wäre besser
        for (int i = 0; i < points.size(); i++) {
            var pb = points.get(i);
            if (pb.time.isAfter(imageTime)) {
                if (i == 0) {
                    System.out.println(imageTime + " outside provided range " + points.get(0).time + " <-> " + points.get(points.size() - 1).time);
                    return null;
                }
                var pa = points.get(i - 1);
                var a = pa.time.toEpochSecond(ZoneOffset.UTC);
                var x = imageTime.toEpochSecond(ZoneOffset.UTC);
                var b = pb.time.toEpochSecond(ZoneOffset.UTC);
                var v = x - a;
                var length = b - a;

                var normalized = (double) v / (double) length; // 0..1
                return new double[]{
                        pa.lat + normalized * (pb.lat - pa.lat),
                        pa.lon + normalized * (pb.lon - pa.lon)
                };
            }
        }
        System.out.println(imageTime + " outside provided range " + points.get(0).time + " <-> " + points.get(points.size() - 1).time);
        return null;
    }

    private static void convertGPXtoCSV() throws IOException {
        GPX gpsData = GPX.read("C:\\dev\\projects\\on1-gpx-tagger\\src\\main\\resources\\2014_02-03 NZ Komplett.gpx");

        try (FileWriter writer = new FileWriter("C:\\Users\\christian\\Desktop\\gps_nz.csv")) {
            writer.write("time,lat,lon,ele,speed\n");

            StringBuilder stringBuilder = new StringBuilder();
            for (var track : gpsData.getTracks()) {
                for (var segment : track.getSegments()) {
                    for (var point : segment.getPoints()) {
                        stringBuilder.setLength(0);
                        writer.write(stringBuilder
                                .append(point.getTime().orElseThrow())
                                .append(",")
                                .append(point.getLatitude().doubleValue())
                                .append(",")
                                .append(point.getLongitude().doubleValue())
                                .append(",")
                                .append(point.getElevation().orElseThrow().doubleValue())
                                .append(",")
                                .append(point.getSpeed().map(x -> Double.toString(x.doubleValue()))
                                        .orElse(""))
                                .append("\n").toString());
                    }
                }
            }
        }
    }

    public static class GpsPoint {
        LocalDateTime time;
        double lat;
        double lon;
        double ele;
        Double speed;

        public GpsPoint(LocalDateTime time, double lat, double lon, double ele, Double speed) {
            this.time = time;
            this.lat = lat;
            this.lon = lon;
            this.ele = ele;
            this.speed = speed;
        }
    }
}

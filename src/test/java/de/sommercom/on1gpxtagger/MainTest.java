package de.sommercom.on1gpxtagger;

import org.geotools.geometry.DirectPosition2D;
import org.geotools.measure.CoordinateFormat;
import org.geotools.referencing.CRS;
import org.junit.jupiter.api.Test;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    public void testLerp() throws IOException {
        List<Main.GpsPoint> points = Main.getPointsFromCSV();
//
//        // TODO: Coverage
//
//        ZonedDateTime start = ZonedDateTime.parse("2014-02-26T21:42:33Z");
//        ZonedDateTime p40 = ZonedDateTime.parse("2014-02-26T21:42:35Z");
//        ZonedDateTime p60 = ZonedDateTime.parse("2014-02-26T21:42:36Z");
//        ZonedDateTime end = ZonedDateTime.parse("2014-02-26T21:42:38Z");
//
////        2014-02-26T21:42:33Z,-36.856407166,174.781021118,66.806366,1.38
////        2014-02-26T21:42:38Z,-36.856361389,174.780975342,63.493027,1.14
//
//        var x1 = -36.856407166 * 0.4 + 0.6 * -36.8563797;
//        var y1 = 174.781021118 * 0.4 + 0.6 * 174.780975342;
//        var x2 = -36.856407166 * 0.6 + 0.4 * -36.8563797;
//        var y2 = 174.781021118 * 0.6 + 0.4 * 174.780975342;
//
//        assertArrayEquals(new double[]{-36.856407166,174.781021118}, Main.getLerpedCoordinateForDate(start, points));
//        assertArrayEquals(new double[]{x1,y1}, Main.getLerpedCoordinateForDate(p40, points), 10E-6);
//        assertArrayEquals(new double[]{x2,y2}, Main.getLerpedCoordinateForDate(p60, points), 10E-5);
//        assertArrayEquals(new double[]{-36.856361389,174.780975342}, Main.getLerpedCoordinateForDate(end, points));
    }

    @Test
    void dmsToDecimal() throws FactoryException {

        final CoordinateReferenceSystem wgs84 = CRS.decode("EPSG:4326");

        CoordinateFormat coordinateFormat = new CoordinateFormat(Locale.US, wgs84);
        coordinateFormat.setAnglePattern("D°M'S.ssssss\" ");
        DirectPosition2D directPosition2D = new DirectPosition2D(wgs84, -36.856578827, 174.783920288);

        String dmsString = coordinateFormat.format(directPosition2D);

        double[] doubles = Main.dmsToDecimal(dmsString);

        assertEquals(directPosition2D.x, doubles[0],1E-9);
        assertEquals(directPosition2D.y, doubles[1], 1E-9);


    }
}